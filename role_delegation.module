<?php

/**
 * @file
 *
 * This module allows site administrators to grant some roles the authority to
 * change roles assigned to users, without them needing the 'administer access
 * control' permission.
 *
 * It provides its own tab in the user profile so that roles can be changed
 * without needing access to the user edit form.
 */

use Drupal\Component\Utility\String;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Url;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function role_delegation_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page':
      $output = '';
      $output = '<p>' . t('This module allows site administrators to grant some roles the authority to assign selected roles to users, without them needing the <em>administer permissions</em> permission.') . '</p>';
      $output .= '<p>' . t('It provides its own tab in the user profile so that roles can be assigned without needing access to the user edit form.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_theme().
 */
function role_delegation_theme() {
  return array(
    'role_delegation_delegate_roles_action_form' => array(
      'render element' => 'form',
    ),
  );
};
