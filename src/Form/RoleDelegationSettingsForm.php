<?php
/**
 * @file
 * Contains \Drupal\role_delegation\Form\RoleDelegationSettingsForm.
 */
namespace Drupal\role_delegation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityManagerInterface;

/**
 * Configure book settings for this site.
 */
class RoleDelegationSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'role_delegation_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $account = $this->entity;
    $roles_current = $account->roles;

    $roles_delegate = array();
    $roles = _role_delegation_roles();
    foreach ($roles as $rid => $role) {
      if (user_access('assign all roles') || user_access("assign $role role")) {
        $roles_delegate[$rid] = isset($form['account']['roles']['#options'][$rid]) ? $form['account']['roles']['#options'][$rid] : $role;
      }
    }

    if (empty($roles_delegate)) {
      // No role can be assigned.
      return;
    }

    if (!isset($form['account'])) {
      $form['account'] = array(
        '#type' => 'value',
        '#value' => $account,
      );
    }

    // Generate the form items.
    $form['account']['roles_change'] = array(
      '#type' => 'checkboxes',
      '#title' => isset($form['account']['roles']['#title']) ? $form['account']['roles']['#title'] : t('Roles'),
      '#options' => $roles_delegate,
      '#default_value' => array_keys(array_intersect_key($roles_current, $roles_delegate)),
      '#description' => isset($form['account']['roles']['#description']) ? $form['account']['roles']['#description'] : t('Change roles assigned to user.'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
