<?php

/**
 * @file
 * Contains \Drupal\role_delegation\RoleDelegationPermissions.
 */

namespace Drupal\user;

use Drupal\Core\Routing\UrlGeneratorTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\Entity;
use Drupal\user\RoleInterface;

/**
 * Defines a class containing permission callbacks.
 */
class RoleDelegationPermissions {

  use StringTranslationTrait;
  use UrlGeneratorTrait;

  /**
   * Returns an array of node type permissions.
   *
   * @return array
   */
  public function rolePermissions() {
    $perms = array();
    // Generate node permissions for all node types.
    foreach (User::getRoles() as $role) {
      $perms += $this->buildPermissions($role);
    }

    return $perms;
  }

  /**
   * Builds a standard list of node permissions for a given type.
   *
   * @param Drupal\user\src\Entity\Role $role
   *   The machine name of role name.
   *
   * @return array
   *   An array of permission names and descriptions.
   */
  protected function buildPermissions(Role $role) {
    $role_name = array('%role_name' => $role->label());

    return array(
      "assign $role role" => array(
        'title' => $this->t('Assign %role role', array('%role' => $role)),
      ),
    );
  }

}
